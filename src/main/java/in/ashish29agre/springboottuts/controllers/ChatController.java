/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.controllers;

import in.ashish29agre.springboottuts.models.Chat;
import in.ashish29agre.springboottuts.repositories.ChatRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ymedia
 */
@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ChatRepository repository;

    @RequestMapping(value = "/newMessage")
    public HttpEntity<Chat> newMessage(@RequestParam("msg") String msg, @RequestParam("author") String author) {

        Chat chat = new Chat();
        if (msg != null && msg.length() > 0) {
            if (author != null && author.length() > 0) {
                chat.setAuthor(author);
            } else {
                chat.setAuthor("Default");
            }
            chat.setCreatedDate(new Date());
            chat.setMsg(msg);
            chat = repository.save(chat);
            return new ResponseEntity(chat, HttpStatus.CREATED);
        } else {
            return new ResponseEntity(null, HttpStatus.FORBIDDEN);
        }

    }

    @RequestMapping("/messages")
    public HttpEntity<Chat> getMessages() {
        List<Chat> chats = repository.findAll(new PageRequest(0, 5, Sort.Direction.DESC, "createdDate")).getContent();
        return new ResponseEntity(chats, HttpStatus.OK);
    }
    
    @RequestMapping("/messages/author")
    public HttpEntity<Chat> getMessagesByAuthor(@RequestParam("author") String author) {
        List<Chat> chats = repository.findMessagesByAuthorAndDateDesc(author);
        return new ResponseEntity(chats, HttpStatus.OK);
    }
    
    @RequestMapping("/message")
    public HttpEntity<Chat> getMessagesByIdAndAuthor(@RequestParam("id") String id, @RequestParam("author") String author) {
        List<Chat> chats = repository.findMessageByIdAndAuthor(id, author);
        return new ResponseEntity(chats, HttpStatus.OK);
    }
    //560a6dcae4b0bf09d10a5860
}
