/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.controllers;

import in.ashish29agre.springboottuts.models.User;
import in.ashish29agre.springboottuts.repositories.UserRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ymedia
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = Logger.getLogger(UserController.class.getSimpleName());

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Map<String, Object> getUsers() {
        List<User> users = userRepository.findAll();
        Map<String, Object> response = new HashMap<>();
        response.put("count", users.size());
        response.put("users", users);
        return response;
    }

    @RequestMapping(method = {RequestMethod.POST})
    public Map<String, Object> create(@RequestBody User user) {
        logger.warning("User is: " + user.getName());
        Map<String, Object> response = new HashMap<>();
        if (user.getName() != null) {
            response.put("message", "success");
            response.put("user", userRepository.save(user));
            return response;
        } else {
            response.put("message", "failure");
            response.put("user", null);
            return response;
        }
    }
}
