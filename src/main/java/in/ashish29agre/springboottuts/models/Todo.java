/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.models;

import org.springframework.data.annotation.Id;

/**
 *
 * @author Ashish
 */
public class Todo {
    
    @Id
    private String id;
    private String item;

    public Todo() {
    }

    
    public Todo(String id, String item) {
        this.id = id;
        this.item = item;
    }

    public Todo(String item) {
        this.item = item;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
    
    
}
