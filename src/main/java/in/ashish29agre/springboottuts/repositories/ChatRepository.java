/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.repositories;

import in.ashish29agre.springboottuts.models.Chat;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 *
 * @author ymedia
 */
public interface ChatRepository extends MongoRepository<Chat, String>{
    @Query(value = "{author: ?0}")
    List<Chat> findMessagesByAuthorAndDateDesc(String author);
    
    
    @Query(value = "{id: ?0, author: ?1}")
    List<Chat> findMessageByIdAndAuthor(String id, String author);
}
