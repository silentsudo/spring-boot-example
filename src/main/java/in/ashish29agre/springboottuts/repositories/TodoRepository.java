/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.springboottuts.repositories;

import in.ashish29agre.springboottuts.models.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author Ashish
 */
public interface TodoRepository extends MongoRepository<Todo, String> {

}
